## [6.4.0] - 2019-03-05
### Library
- Packaged with `lib 6.4.18`
### Fixed
- Join event triggered too soon listening timeupdate

## [6.2.0] - 2018-06-12
### Library
- Packaged with `lib 6.2.6`
