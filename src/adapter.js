var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.AzureMediaPlayer = youbora.Adapter.extend({
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  getPlayhead: function () {
    if (this.player.currentMediaTime && typeof this.player.currentMediaTime() === "number") {
      return this.player.currentMediaTime()
    }
    return this.player.cache_.currentTime || 0
  },

  getPlayrate: function () {
    if (this.flags.isPaused) {
      return 0
    }
    if (this.player.tag) {
      return this.player.tag.playbackRate
    } else if (this.player.tech_ && this.player.tech_.mediaPlayer) {
      return this.player.tech_.mediaPlayer.playbackRate()
    }
    return 1
  },

  getDroppedFrames: function () {
    var playertag = this.player.mediaPlayer || this.player.tech_.mediaPlayer
    if (playertag) return playertag.webkitDroppedFrameCount;
    return null
  },

  getDuration: function () {
    if (this.player.duration()) {
      return this.player.duration()
    }
    return null
  },

  getBitrate: function () {
    return this.player.currentPlaybackBitrate() || -1
  },

  getThroughput: function () {
    var playertag = this.player.mediaPlayer || this.player.tech_.mediaPlayer
    if (playertag && playertag.videoBufferData) {
      return playertag.videoBufferData._downloadCompleted.measuredBandwidth
    }
    return null
  },

  getRendition: function () {
    var playertag = this.player.mediaPlayer || this.player.tech_.videoTag_ || this.player.tech_.mediaPlayer
    if (playertag && playertag.videoWidth && playertag.videoHeight) {
      return youbora.Util.buildRenditionString(playertag.videoWidth,
        playertag.videoHeight, this.getBitrate())
    }
    return youbora.Util.buildRenditionString(this.getBitrate())
  },

  getTitle: function () {
    return this.player._title || "unknown"
  },

  getIsLive: function () {
    return this.player.isLive()
  },

  getResource: function () {
    return this.player.currentSrc()
  },

  getPlayerName: function () {
    return "Azure Media Player"
  },

  getPlayerVersion: function () {
    return this.player.getAmpVersion()
  },

  registerListeners: function () {
    this.monitorPlayhead(true, false)
    if (amp) {
      this.references = []
      this.references[amp.eventName.play] = this.playListener.bind(this)
      this.references[amp.eventName.loadedmetadata] = this.playListener.bind(this)
      this.references[amp.eventName.pause] = this.pauseListener.bind(this)
      this.references[amp.eventName.playing] = this.playingListener.bind(this)
      this.references[amp.eventName.error] = this.errorListener.bind(this)
      this.references[amp.eventName.seeking] = this.seekingListener.bind(this)
      this.references[amp.eventName.seeked] = this.seekedListener.bind(this)
      this.references[amp.eventName.ended] = this.endedListener.bind(this)
      this.references[amp.eventName.timeupdate] = this.timeupdateListener.bind(this)
      this.references[amp.eventName.waiting] = this.bufferingListener.bind(this)
      for (var key in this.references) {
        this.player.addEventListener(key, this.references[key])
      }
    }
  },

  unregisterListeners: function () {
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.removeEventListener(key, this.references[key])
      }
      this.references = []
    }
  },

  playListener: function (e) {
    if (!this.player.error()) {
      if (!this.flags.isStarted) {
        this.fireStart()
        this.playingTriggered = false
      }
    }
  },

  pauseListener: function (e) {
    if (!this.flags.isBuffering && !this.flags.isSeeking) this.firePause()
  },

  playingListener: function (e) {
    this.playingTriggered = true
    this.fireResume()
    this.fireSeekEnd()
    this.fireBufferEnd()
    this.fireJoin()
  },

  errorListener: function (e) {
    if (this.player.error && this.player.error()) {
      var error = this.player.error()
      this.fireError(error.code, error.message)
    } else {
      this.fireError()
    }
    this.fireStop()
    this.playingTriggered = false
  },

  seekingListener: function (e) {
    this.fireSeekBegin()
  },

  seekedListener: function (e) {
    this.fireSeekEnd()
  },

  endedListener: function (e) {
    this.fireStop()
    this.playingTriggered = false
  },

  timeupdateListener: function (e) {
    if (!this.flags.isJoined) {
      if (!this.player.error()) {
        this.fireStart()
      }
      if (this.playingTriggered) this.fireJoin()
    }
  },

  bufferingListener: function (e) {
    if (!this.flags.isSeeking) this.fireBufferBegin()
  }
})

module.exports = youbora.adapters.AzureMediaPlayer
